#! /bin/sh

echo "Testing start:"
echo "Testing: test01"
./pagerank 2 < tests/test01.in | diff - tests/test01.out
echo "Testing: test02"
./pagerank 2 < tests/test02.in | diff - tests/test02.out
echo "Testing: test03"
./pagerank 2 < tests/test03.in | diff - tests/test03.out
echo "Testing: test04"
./pagerank 2 < tests/test04.in | diff - tests/test04.out
echo "Testing: test05"
./pagerank 2 < tests/test05.in | diff - tests/test05.out
echo "Testing: test06"
./pagerank 2 < tests/test06.in | diff - tests/test06.out
echo "Testing: test07"
./pagerank 2 < tests/test07.in | diff - tests/test07.out
echo "Testing: test08"
./pagerank 2 < tests/test08.in | diff - tests/test08.out
echo "Testing: test09"
./pagerank 2 < tests/test09.in | diff - tests/test09.out
echo "Testing: test10"
./pagerank 2 < tests/test10.in | diff - tests/test10.out
echo "Testing: test11"
./pagerank 2 < tests/test11.in | diff - tests/test11.out
echo "Testing: test12"
./pagerank 8 < tests/test12.in | diff - tests/test12.out
echo "Testing done."