#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <immintrin.h>

#include "pagerank.h"

// STRUCTS: -------------------------------------------------------------------
struct v {
  int npages;
  double *matrix_m;
  double *prev_vec_p;
  double *vec_p;
  int from;
  int to;
  double* vec_norm;   
};
struct w {
   node *head;
   int npages;
   int from;
   int to;
   double *matrix_m;
   double ldv; //l_dampener*val
   double add; //add
   double ld;  //l_dampener 
   node *tmp_node;
};
// STRUCTS: -------------------------------------------------------------------

// THREAD FUNCTIONS: ----------------------------------------------------------
// Thread function
void * function(void* param) {
    struct v *data = param;
    for (int i = data->from; i < data->to; i++) {
        double sum = 0.0;
        for (int j = 0; j < data->npages; j++) { 
            sum += data->matrix_m[i*data->npages + j] * data->prev_vec_p[j]; 
        }
        data->vec_p[i] = sum;
		double inter = (data->vec_p[i]-data->prev_vec_p[i]);
		*data->vec_norm += (inter * inter);
	}
	 
    pthread_exit(0);
}
// Thread function
void * set_m(void* param) {
    struct w *data = param;
    node *current = data->head;
    for (int j = 0; j < data->npages; j++) {
        if (current->page->noutlinks == 0) {
            for (int i = data->from; i < data->to; i++) {
                data->matrix_m[i*data->npages + j] = data->ldv + data->add;
            }
        } else {
            node* tmp_node = data->tmp_node;
            for (int i = data->from; i < data->to; i++) {
                node* in_node = tmp_node->page->inlinks;
                int has_changed = 0;
                while (in_node != NULL) {
                    if (in_node->page->index == current->page->index) {
                        data->matrix_m[i*data->npages + j] = data->ld/(double)current->page->noutlinks + data->add;
                        has_changed = 1;
                        break;
                    } else {
                        in_node = in_node->next;
                    }
                }   
                if (!has_changed) {
                    data->matrix_m[i*data->npages + j] = data->add;
                }
                tmp_node = tmp_node->next;
            }
        }
		
        current = current->next;
    }
    
    pthread_exit(0);
}
// THREAD FUNCTIONS: ----------------------------------------------------------

void pagerank(node* list, size_t npages, size_t nedges, size_t nthreads, double dampener) {
	
	const int l_npages = npages;
	const double l_dampener = dampener;
	const double elements = l_npages * l_npages;
	const double val = 1.0/(double)l_npages;
	const double add = ((1.0-l_dampener)/((double)l_npages));

	// Constructing an empty matrix M of doubles, for extra precision when computing.
	double* matrix_m = (double*)malloc(elements*sizeof(double));
    /*
    * Fill the matrix with values according to:
    *
	* 		      | 1/N         if |OUT(j)| == 0
	* 		Mij = | 1/|OUT(j)|  if i links to j.
	*			  | 0           otherwise.
    *
    */
	pthread_t threads[nthreads-1];
	if (nthreads == 1) {
		node *current = list;
		// For each column in the matrix.
		for (int j = 0; j < l_npages; j++) {
			// If the number of outlinks of the node is 0, make whole column = 1/l_npages. 
			if (current->page->noutlinks == 0) {
				for (int i = l_npages; i--;) {
					matrix_m[i*l_npages + j] = l_dampener*val + add;
				}
			}
			// Else,
			else {
				node* tmp_node = list;
				for (int i = 0; i < l_npages; i++) {
					node* in_node = tmp_node->page->inlinks;
					int has_changed = 0;
					while (in_node != NULL) {
						if (in_node->page->index == current->page->index) {
							matrix_m[i*l_npages + j] = l_dampener/(double)current->page->noutlinks + add;
							has_changed = 1;
							break;
						} else {
							in_node = in_node->next;
						}
					}
					if (!has_changed) {
						matrix_m[i*l_npages + j] = add;
					}
					tmp_node = tmp_node->next;
				}	
			}
			current = current->next;
		}		
	} else {
		for (int i = 1; i < nthreads; i++) {
    	    struct w *data = (struct w *)malloc(sizeof(struct w));
    	    
    	    data->head     = list;
    	    data->npages   = l_npages;
    	    data->from     = (l_npages/nthreads)*i;
    	    if (i+1 == nthreads) {
    	        data->to   = l_npages;
    	    } else {
    	        data->to   = (l_npages/nthreads)*(i+1);
    	    }    
    	    data->matrix_m = matrix_m;
    	    data->ldv      = l_dampener*val;
    	    data->add      = add;
    	    data->ld       = l_dampener;
			node* itr = list;
        	for (int i = 0; i < data->from; i++) {
        	    itr = itr->next;
        	}
        	data->tmp_node = itr;
    	    pthread_create(&threads[i-1], NULL, set_m, data);
    	}
    	
    	// Then the main thread does its' bit
    	node *current = list;
    	for (int j = 0; j < l_npages; j++) {
    	    if (current->page->noutlinks == 0) {
    	        for (int i = 0; i*nthreads < l_npages; i++) {
    	            matrix_m[i*l_npages + j] = l_dampener*val + add;
    	        }
    	    } else {
    	        node* tmp_node = list;
    	        for (int i = 0; i*nthreads < l_npages; i++) {
    	            node* in_node = tmp_node->page->inlinks;
    	            int has_changed = 0;
    	            while (in_node != NULL) {
    	                if (in_node->page->index == current->page->index) {
    	                    matrix_m[i*l_npages + j] = l_dampener/(double)current->page->noutlinks + add;
    	                    has_changed = 1;
    	                    break;
    	                } else {
    	                    in_node = in_node->next;
    	                }
    	            }   
    	            if (!has_changed) {
    	                matrix_m[i*l_npages + j] = add;
    	            }
    	            tmp_node = tmp_node->next;
    	        }
    	    }
    	    current = current->next;
    	}
    	
    	for (int i = 0; i < nthreads-1; i++) {
    	    pthread_join(threads[i], NULL);
    	}
	}
	
	// Constructing an empty vector P, which will be P0 at this point.
	double vector_p[l_npages];
    for (int i = l_npages; i--;) {
        vector_p[i] = val;
    }
	
	/*
	* Iteratively do the following:
	*  - Calculate P(t+1) by the following:
	*  		- P(t+1) = (M^)(P(t)) 
	*  - Loop until || P(t+1) - P(t) || <= epsilon,
	*    Where:
	*  		- epsilon = 0.005 (Defined in header file.)
	*		- ||x|| = sqrt(sigma(x^2)) on a vector x = {x1, x2, ..., xn}
	*		  (The square root of the sum of the square of each element in the vector.)
	*/
	double vec_norm = 1;
	double prev_vec_p[l_npages];
	while (vec_norm > EPSILON) {
		for (int i = l_npages; i--;) {
			prev_vec_p[i] = vector_p[i];
		}
		vec_norm = 0.0;
		// Matrix multiplication on matrix_m_hat and vector_p.
		if (nthreads == 1) {
			for (int i = l_npages; i--;) {
				double sum = 0.0;
				for (int j = l_npages; j--;) {
					sum += matrix_m[i*l_npages + j] * prev_vec_p[j];
				}
				vector_p[i] = sum;
				double inter = (vector_p[i]-prev_vec_p[i]);
				vec_norm += (inter * inter);			
			}

		} else {
    		
    		for (int i = 1; i < nthreads; i++) {
    			struct v *data = (struct v *)malloc(sizeof(struct v));
    		       
    		    data->npages       = l_npages;
    		    data->matrix_m     = matrix_m;
    		    data->prev_vec_p   = prev_vec_p;
    		    data->vec_p        = vector_p;
    		    data->from         = (l_npages/nthreads)*i;
				if (i+1 == nthreads) {
					data->to       = l_npages;
				} else {
    		    	data->to       = (l_npages/nthreads)*(i+1);
				}
				data->vec_norm     = &vec_norm;
    		    pthread_create(&threads[i-1], NULL, function, data);
    		}
			
			// Then the main thread does its' bit.
			for (int i = 0; i * nthreads < l_npages; i++) {
        		double sum = 0.0;
        		for (int j = 0; j < l_npages; j++) { 
		            sum += matrix_m[i*l_npages + j] * prev_vec_p[j]; 
        		}
        		vector_p[i] = sum;
			}
			double inter;
			for (int i = 0; i * nthreads < l_npages; i++) {
				inter = (vector_p[i]-prev_vec_p[i]);
				vec_norm += (inter * inter);
			} 
        	   
    		for (int i = 0; i < nthreads-1; i++) {
    		    pthread_join(threads[i], NULL);
    		}
			
		}		
		
		// Calculating Euclidean vector norm.
		//vec_norm = 0.0;
		//double inter;
		//for (int i = l_npages; i--;) {
		//	inter = (vector_p[i]-prev_vec_p[i]);
		//	vec_norm += (inter * inter);
		//}
		vec_norm = sqrt(vec_norm);
	}

	free(matrix_m); // No longer needed.
	
	// Now, convergence threshold has been reached, print results.
	node* cur_p = list;
	for (int i = 0; i < l_npages; i++) {
    	page tmp_page = *cur_p->page;
		printf("%s %.8lf\n", tmp_page.name, vector_p[i]);
		cur_p = cur_p->next;
	}

	
}

/*
######################################
### DO NOT MODIFY BELOW THIS POINT ###
######################################
*/

int main(int argc, char** argv) {

	/*
	######################################################
	### DO NOT MODIFY THE MAIN FUNCTION OR HEADER FILE ###
	######################################################
	*/

	config conf;

	init(&conf, argc, argv);

	node* list = conf.list;
	size_t npages = conf.npages;
	size_t nedges = conf.nedges;
	size_t nthreads = conf.nthreads;
	double dampener = conf.dampener;

	pagerank(list, npages, nedges, nthreads, dampener);

	release(list);

	return 0;
}
