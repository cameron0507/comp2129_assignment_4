#include <stdio.h>
#include <immintrin.h>

int main(void) {
    
    float mA[16];
    float vB[4];
    float vC[4];
    
    for(int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            mA[i*4+j] = i+j;    
        }
        
        vB[i] = 2*i;
        vC[i] = 0.0;
    }
    
    /*
    for (int i = 0; i < 4; i++) {
        float sum = 0.0;
        for (int j = 0; j < 4; j++) {
            sum += mA[i*4+j] * vB[j];
        }
        vC[i] = sum;
    }
    */
    float tmp0[4] = {0.0,0.0,0.0,0.0};
    float tmp1[4] = {0.0,0.0,0.0,0.0};
    float tmp2[4] = {0.0,0.0,0.0,0.0};
    float tmp3[4] = {0.0,0.0,0.0,0.0};
    float tmp4[4] = {0.0,0.0,0.0,0.0};
    float one = 1.0;
    float random[4] = {0.0,0.0,0.0,0.0};

    __m128 xmm0, xmm1, ymm2, ymm3, ymm4, ymm5;
    ymm4 = _mm256_loadu_ps(vB);
    ymm5 = _mm256_broadcast_ss(&one);
    ymm0 = _mm256_loadu_ps(&mA[0]);
    ymm1 = _mm256_loadu_ps(&mA[4]);
    ymm2 = _mm256_loadu_ps(&mA[8]);
    ymm3 = _mm256_loadu_ps(&mA[12]);
    
    ymm0 = _mm256_mul_ps(ymm0, ymm4);
    _mm256_storeu_ps(random, ymm0);
    printf("random:\n");
    for (int i = 0; i < 4; i++) {
        printf("%f ", random[i]);
    }
    printf("\n");
    ymm1 = _mm256_mul_ps(ymm1, ymm4);
    _mm256_storeu_ps(random, ymm1);
    printf("random:\n");
    for (int i = 0; i < 4; i++) {
        printf("%f ", random[i]);
    }
    printf("\n");
    ymm2 = _mm256_mul_ps(ymm2, ymm4);
    _mm256_storeu_ps(random, ymm2);
    printf("random:\n");
    for (int i = 0; i < 4; i++) {
        printf("%f ", random[i]);
    }
    printf("\n");
    ymm3 = _mm256_mul_ps(ymm3, ymm4);
    _mm256_storeu_ps(random, ymm3);
    printf("random:\n");
    for (int i = 0; i < 4; i++) {
        printf("%f ", random[i]);
    }
    printf("\n");
    
    ymm0 = _mm256_dp_ps(ymm0, ymm5, 0xFF);
    ymm1 = _mm256_dp_ps(ymm1, ymm5, 0xFF);
    ymm2 = _mm256_dp_ps(ymm2, ymm5, 0xFF);
    ymm3 = _mm256_dp_ps(ymm3, ymm5, 0xFF);
    
    _mm256_storeu_ps(tmp0, ymm0);
    _mm256_storeu_ps(tmp1, ymm1);
    _mm256_storeu_ps(tmp2, ymm2);
    _mm256_storeu_ps(tmp3, ymm3);
    _mm256_storeu_ps(tmp4, ymm5);
    
    for (int i = 0; i < 4; i++) {
        printf("%f ", tmp0[i]);
    }
    printf("\n");
    for (int i = 0; i < 4; i++) {
        printf("%f ", tmp1[i]);
    }
    printf("\n");
    for (int i = 0; i < 4; i++) {
        printf("%f ", tmp2[i]);
    }
    printf("\n");
    for (int i = 0; i < 4; i++) {
        printf("%f ", tmp3[i]);
    }
    printf("\n");
    
    vC[0] = tmp0[0];
    vC[1] = tmp1[0];
    vC[2] = tmp2[0];
    vC[3] = tmp3[0];
    
    printf("tmp4\n");
    printf("%f ", tmp4[0]);
    printf("%f ", tmp4[1]);
    printf("%f ", tmp4[2]);
    printf("%f ", tmp4[3]);
    printf("\n");
    
    // Print the matrices.
    printf("Matrix A:\n");
    for (int i = 0; i < 16; i++) {
        printf("%f ", mA[i]);
    }
    printf("\n");
    printf("Vector B:\n");
    for (int i = 0; i < 4; i++) {
        printf("%f ", vB[i]);
    }
    printf("\n");
    printf("Vector C:\n");
    for (int i = 0; i < 4; i++) {
        printf("%f ", vC[i]);
    }
    printf("\n");
    
}